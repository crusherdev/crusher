export interface iReactSelectOption {
	label: string;
	value: string;
}
